﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.UI;

public class expensesCalc : MonoBehaviour {

    // Expenses Area
    public GameObject performerCostText;
    public GameObject venueCostText;
    public GameObject eventCostText;
    public GameObject suppliesCostText;

    // Income Area
    public GameObject performanceIncomeText;
    public GameObject fundraiserIncomeText;
    public GameObject sponsorsIncomeText;
    public GameObject donationsIncomeText;

    public int supplyCost;
    public int rehearsal_count;
    public int performance_count;

    public List<charaClass.Character> performer_list;
    
    void Start() {
        string performer_path = Application.streamingAssetsPath + "/Creature.json";

        string performer_json = File.ReadAllText(performer_path);

        performer_list = JsonConvert.DeserializeObject<List<charaClass.Character>>(performer_json);

        performerCostText.GetComponent<Text>().text = "$" + orchestra_cost(performer_list, rehearsal_count, performance_count).ToString();
        venueCostText.GetComponent<Text>().text = "$" + venue_cost(practice_hall_cost(rehearsal_count), recital_hall_cost(performance_count)).ToString();
        eventCostText.GetComponent<Text>().text = "$0";
    }

    int performer_cost(charaClass.Character performer, int rehersal_count, int perf_count) {
        int[] conmaster = { 75, 100 };
        int[] principal = { 60, 85 };
        int[] normal = { 40, 50 };
        int total = 0;

        if(performer.Position == 0) {
            total += (rehearsal_count * conmaster[0]);
            total += (performance_count * conmaster[1]);
        } else if (performer.Position == 1) {
            total += (rehearsal_count * principal[0]);
            total += (performance_count * principal[1]);
        } else {
            total += (rehearsal_count * normal[0]);
            total += (performance_count * normal[1]);
        }
        return total;
    }

    int orchestra_cost(List<charaClass.Character> performer_list, int rehersal_count, int performance_count) {
        int total = 0;
        foreach(charaClass.Character npc in performer_list) {
            total += performer_cost(npc, rehersal_count, performance_count);
        }
        return total;
    }

    int practice_hall_cost(int rehearsal_count) {
        int rehearsal_cost = 100;

        return rehearsal_cost * rehearsal_count;
    }

    int recital_hall_cost(int performance_count) {
        int performance_cost = 100;

        return performance_cost * performance_count;
    }

    int venue_cost(int practice_hall, int recital_hall) {
        int total = practice_hall + recital_hall;

        return total;
    }

    int total_cost(List<charaClass.Character> NPCs, int rehearsal_count, int performance_count) {
        int total = 0;
        int recital_hall = recital_hall_cost(performance_count);
        int practice_hall = practice_hall_cost(rehearsal_count);

        total += orchestra_cost(NPCs, rehearsal_count, performance_count);
        total += venue_cost(practice_hall, recital_hall);

        return total;
    }
}
