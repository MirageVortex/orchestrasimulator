﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuToggle : MonoBehaviour {

    public GameObject computerMenu;
    public GameObject bedMenu;
    public GameObject doorMenu;
    public GameObject managerMenu;
    public GameObject repitoireMenu;
    public GameObject auditionMenu;
    public GameObject buyMenu;
    public GameObject statsMenu;

    List<GameObject> MenuList;

	// Use this for initialization
	void Start () {
        computerMenu.SetActive(false);
        bedMenu.SetActive(false);
        doorMenu.SetActive(false);
        managerMenu.SetActive(false);
        repitoireMenu.SetActive(false);
        auditionMenu.SetActive(false);
        buyMenu.SetActive(false);

        MenuList = new List<GameObject>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (MenuList.Count != 0) {
                MenuList[0].SetActive(false);
                MenuList.RemoveAt(0);
                Time.timeScale = 1;
            } else {
                MenuList.Add(statsMenu);
                openMenu(statsMenu);
            }
        }

        if (Time.timeScale == 0) {
            bedMenu.SetActive(false);
            doorMenu.SetActive(false);
        }
	}

    private void OnMouseDown () {
        if (gameObject.name == "Computer") {
            computerMenu.SetActive(true);
            bedMenu.SetActive(false);
            doorMenu.SetActive(false);
        } else if (gameObject.name == "Door") {
            doorMenu.SetActive(true);
            bedMenu.SetActive(false);
            computerMenu.SetActive(false);
        } else if (gameObject.name == "Bed") {
            bedMenu.SetActive(true);
            doorMenu.SetActive(false);
            computerMenu.SetActive(false);
        }
    }

    public void openMenu (GameObject menu) {
        menu.SetActive(true);
        MenuList.Add(menu);

        Time.timeScale = 0;
    }

    public void closeMenu (GameObject menu) {
        menu.SetActive(false);
    }

    //public void openStatsMenu (int index)
    //{
    //    statsMenu.
    //}
}
