﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerPause : MonoBehaviour {

	private bool Pause;
	public GameObject cameraObject;

	// Setting Pause state
	void Start () {
		Pause = false;
		Time.timeScale = 1;
		cameraObject.GetComponent<cameraMove>().enabled = true;
	}
	
	// Pressing "esc" pauses and unpauses
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (Pause == false) {
				Pause = true;
				Time.timeScale = 0;
				print ("pause");
				cameraObject.GetComponent<cameraMove>().enabled = false;
			} else {
			Pause = false;
			Time.timeScale = 1;
			print ("go");
			cameraObject.GetComponent<cameraMove>().enabled = true;
			}
		}
	}
}