﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseGame : MonoBehaviour {


    // Use this for initialization
    public GameObject menu;
    // public GameObject save_menu;

	void Start () {
        Time.timeScale = 1;
        menu.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape) && !menu.activeSelf) {
            Time.timeScale = 0;
            menu.SetActive(true);
                // print("game: paused"); // for testing purposes;
                // disable input
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && menu.activeSelf){
            Time.timeScale = 1;
            menu.SetActive(false);
            // print("game: resumed"); // for testing purposes;
        }
        
	}
}
