﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.UI;

public class statsMenu : MonoBehaviour {

    public int charaIndex;
    public GameObject menu;
	public GameObject characterInfo;

	public GameObject nameText;
    public GameObject genderText;
	public GameObject ageText;
	public GameObject instrumentText;
    //public GameObject musicalityText;
    //public GameObject charismaText;
    //public GameObject diligenceText;
    //public GameObject prestigeText;
    //public GameObject experienceText;

    private List<charaClass.Character> charaList;
    private string[] InstrumentList;
    private string[] GenderList;
    private List<charaClass.Character> playerList;

    // Use this for initialization
    void Start() {
        InstrumentList = characterInfo.GetComponent<itemList>().InstrumentList;
        GenderList = characterInfo.GetComponent<itemList>().GenderList;

        string path = Application.streamingAssetsPath + "/CharaStats.json";
        string jsonString = File.ReadAllText(path);

        playerList = JsonConvert.DeserializeObject<List<charaClass.Character>>(jsonString);
    }

    void Update() {
        // Player Stats
        UpdateText();
    }

    public void OnMouseDown(int NewIndex) {
        charaIndex = NewIndex;
        menu.SetActive(true);
    }

    void EscapeClick() {
        if (!menu.activeSelf) {
            menu.SetActive(true);
        } else {
            menu.SetActive(false);
        }
    }

    void UpdateText()
    {
        nameText.GetComponent<Text>().text = playerList[charaIndex].FirstName.ToString();
        genderText.GetComponent<Text>().text = GenderList[playerList[charaIndex].Gender];
        string age = playerList[charaIndex].Age.ToString() + " Years Old";
        ageText.GetComponent<Text>().text = age;
        instrumentText.GetComponent<Text>().text = InstrumentList[playerList[charaIndex].Instrument];
        //musicalityText.GetComponent<Text>().text = playerList[charaIndex].Stats[0].ToString();
        //charismaText.GetComponent<Text>().text = playerList[charaIndex].Stats[1].ToString();
        //diligenceText.GetComponent<Text>().text = playerList[charaIndex].Stats[2].ToString();
        //prestigeText.GetComponent<Text>().text = playerList[charaIndex].Stats[3].ToString();
        //experienceText.GetComponent<Text>().text = playerList[charaIndex].Stats[4].ToString();
    }
}