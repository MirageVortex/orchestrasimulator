﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class auditionMenu : MonoBehaviour {

    public GameObject itemListObj;
    public GameObject statsFunct;

    public Transform Audition_List;
    public Transform NPC_List;

    private string[] maleNames;
    private string[] femaleNames;
    private string[] lastNames;
    private string[] genderList;
    private string[] instrumentList;
    private int[] instrumentListCount;

    private List<charaClass.Character> tempCharaList = new List<charaClass.Character>();
    private List<charaClass.Character> CharaList;

    public int charaIndex;
    public GameObject menu;
    public Button hireButton;

    public GameObject nameText;
    public GameObject genderText;
    public GameObject ageText;
    public GameObject instrumentText;

    // Use this for initialization
    void Start()
    {
        // Get the list of game data (names, instruments, etc)
        maleNames = itemListObj.GetComponent<itemList>().MaleNamesList;
        femaleNames = itemListObj.GetComponent<itemList>().FemaleNameList;
        lastNames = itemListObj.GetComponent<itemList>().LastNameList;
        genderList = itemListObj.GetComponent<itemList>().GenderList;
        instrumentList = itemListObj.GetComponent<itemList>().InstrumentList;
        instrumentListCount = itemListObj.GetComponent<itemList>().OrcheInstrumentList;

        // Load in current playerList
        string path = Application.streamingAssetsPath + "/CharaStats.json";
        string jsonString = File.ReadAllText(path);

        CharaList = JsonConvert.DeserializeObject<List<charaClass.Character>>(jsonString);

        // Player chooses instrument
        int playerInstrument = Random.Range(0, 12);

        // Create the new character

        // Remove an instrument slot for the player
        instrumentListCount[playerInstrument]--;

        // Create NPCS randomly
        for (int i = 0; i < instrumentListCount.Length; i++)
        {
            for (int num = 0; num < instrumentListCount[i] * 2; num++)
            {
                int firstName = Random.Range(0, 100);
                int lastName = Random.Range(0, 26);
                int Age = Random.Range(27, 51); //Random Age between 27 and 50
                int Gender = Random.Range(0, 2);
                int Rank = statsFunct.GetComponent<statsFunctions>().GetRank();
                int Position = 0;

                tempCharaList.Add(statsFunct.GetComponent<statsFunctions>().CreateNPC(firstName, lastName, Age, Gender, i, Rank, Position));
            }
        }
    }

    void Update()
    {
        UpdateAuditionMenu();
        UpdateCurrentMemberMenu();

        // Player Stats
        if (CharaList.Contains(tempCharaList[charaIndex]))
        {
            hireButton.gameObject.SetActive(false);
        }
        else
        {
            hireButton.gameObject.SetActive(true);
        }
    }

    public void HireNPC ()
    {
        CharaList.Add(tempCharaList[charaIndex]);
    }

    public void save (List<charaClass.Character> charaList)
    {
        statsFunct.GetComponent<statsFunctions>().npcJSONSave(charaList);
    }

    public void OpenMenu(int NewIndex)
    {
        charaIndex = NewIndex;
        menu.SetActive(true);
        UpdateAuditionText();
    }

    public void OpenCurrentMenu (int NewIndex)
    {
        charaIndex = NewIndex;
        menu.SetActive(true);
        UpdateText();
    }

    void UpdateAuditionText()
    {
        nameText.GetComponent<Text>().text = tempCharaList[charaIndex].FirstName.ToString() + " " + tempCharaList[charaIndex].LastName.ToString();
        genderText.GetComponent<Text>().text = genderList[tempCharaList[charaIndex].Gender];
        string age = tempCharaList[charaIndex].Age.ToString() + " Years Old";
        ageText.GetComponent<Text>().text = age;
        instrumentText.GetComponent<Text>().text = instrumentList[tempCharaList[charaIndex].Instrument];
    }

    void UpdateAuditionMenu()
    {
        for (int i = 0; i < tempCharaList.Count; i++)
        {
            if (Audition_List.GetChild(i).name.Substring(0, 3) == "NPC")
            {
                Audition_List.GetChild(i).GetChild(0).GetComponent<Text>().text = tempCharaList[i].FirstName;
                Audition_List.GetChild(i).GetChild(1).GetComponent<Text>().text = instrumentList[tempCharaList[i].Instrument];
                Audition_List.GetChild(i).GetChild(2).GetComponent<Text>().text = tempCharaList[i].Rank.ToString();
            }
        }
    }

    void UpdateText()
    {
        nameText.GetComponent<Text>().text = CharaList[charaIndex].FirstName.ToString() + " " + CharaList[charaIndex].LastName.ToString();
        genderText.GetComponent<Text>().text = genderList[CharaList[charaIndex].Gender];
        string age = CharaList[charaIndex].Age.ToString() + " Years Old";
        ageText.GetComponent<Text>().text = age;
        instrumentText.GetComponent<Text>().text = instrumentList[CharaList[charaIndex].Instrument];
    }

    void UpdateCurrentMemberMenu()
    {
        for (int i = 1; i < CharaList.Count; i++)
        {
            if (NPC_List.GetChild(i).name.Substring(0, 3) == "NPC")
            {
                NPC_List.GetChild(i-1).GetChild(0).GetComponent<Text>().text = CharaList[i].FirstName;
                NPC_List.GetChild(i-1).GetChild(1).GetComponent<Text>().text = instrumentList[CharaList[i].Instrument];
                NPC_List.GetChild(i-1).GetChild(2).GetComponent<Text>().text = CharaList[i].Rank.ToString();
            }
        }
    }



}
