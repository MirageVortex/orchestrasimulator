﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class statsFunctions : MonoBehaviour {

    public GameObject itemListObject;


    string[] InstrumentList;
    string[] maleNames;
    string[] femaleNames;
    string[] lastNames;

    bool debug = false;

    void Awake() {
        InstrumentList = itemListObject.GetComponent<itemList>().InstrumentList;
        maleNames = itemListObject.GetComponent<itemList>().MaleNamesList;
        femaleNames = itemListObject.GetComponent<itemList>().FemaleNameList;
        lastNames = itemListObject.GetComponent<itemList>().LastNameList;
    }

    public int GetRank() {
        // Generates a rank using the Random.Range function
        // 1-15 || 16-60 || 61-85 || 86-95 || 96-100
        int random_int = Random.Range(1, 101); // Cuz Max is exclusive
        
        if (random_int <= 15) {
            return 1;
        } else if (random_int > 15 && random_int <= 60) {
            return 2;
        } else if (random_int > 60 && random_int <= 92) {
            return 3;
        } else if (random_int > 93 && random_int <= 98) {
            return 4;
        } else {
            return 5;
        }
    }

    public int[] GenerateStats (int Rank) {
        // Generate stats based off of the Character's Rank
        //------------PRE ADJUSTED TOTALS------------------
        // 75 || 150 || 250 || 375 || 480
        //--------------- ADJUSTED ------------------------
        // 70 || 145 || 245 || 370 || 475
        int[] Stats = { 1, 1, 1, 1, 1 }; // Minimum Stats is 1;
        int TotalSP = 70; // Total Stat Points (initial)

        // Add more Total Stat Points based off of the Rank
        if (Rank == 2){
            TotalSP += 75;
        } else if (Rank == 3) {
            TotalSP += 175;
        } else if (Rank == 4) {
            TotalSP += 300;
        } else if (Rank == 5) {
            TotalSP += 405;
        }

        int points = 0; // Initialize the integar points variable
        // Start Distributing the points to the stats;
        while (TotalSP > 0) {
            for (int i = 0; i< 5; i++) {
                if (TotalSP > StatCap(Rank)) {
                    points = ConfigurePoints(Stats[i], Random.Range(0, StatCap(Rank)), Rank);
                    Stats[i] += points;
                    TotalSP -= points;
                } else {
                    points = ConfigurePoints(Stats[i], Random.Range(0, TotalSP+1), Rank);
                    Stats[i] += points;
                    TotalSP -= points;
                }
            }
        }
        return Stats;
    }

    public int ConfigurePoints (int StatPoints, int TempPoints, int Rank) {
        // Check if Total is greater than the current Stat Cap for the Rank
        int Total = StatPoints + TempPoints;
        if (Total > StatCap(Rank)) {
            int leftover = Total - StatCap(Rank);
            TempPoints -= leftover;
        }
        return TempPoints;
    }

    public int StatCap (int Rank) {
        // Stat Caps for character Ranks
        // 30 || 40 || 55 || 75 || 100
        if (Rank == 1) {
            return 25;
        } else if (Rank == 2) {
            return 40;
        } else if (Rank == 3) {
            return 60;
        } else if (Rank == 4) {
            return 80;
        } else {
            return 100;
        }
        
    }

    public charaClass.Character CreateNPC(int firstName, int lastName, int Age, int Gender, int Instrument, int Rank, int Position) {
        // Creates a random NPC Character Class.
        // Generate random stats
        int[] Stats = GenerateStats(Rank);

        // if male (use male names)
        // Create Character
        if (Gender == 0) {
            return new charaClass.Character(maleNames[firstName], lastNames[lastName], Age, Gender, Instrument, Stats, Rank, Position);
        // if female (use female names)
        } else {
            return new charaClass.Character(femaleNames[firstName], lastNames[lastName], Age, Gender, Instrument, Stats, Rank, Position);
        }
    }

    public void npcJSONSave(List<charaClass.Character> chars)
    {
        // Set the save path to CharaStats.json in the StreamingAsset Folder
        string savePath = Application.streamingAssetsPath + "/CharaStats.json";

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < chars.Count; i++)
        {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(chars[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void playerJSONSave(List<playerClass.Player> player)
    {
        // Set the save path to CharaStats.json in the StreamingAsset Folder
        string savePath = Application.streamingAssetsPath + "/Player.json";

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < player.Count; i++)
        {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(player[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void concertHallJSONSave(List<ConcertHallClass.ConcertHall> concertHalls)
    {
        // Set the save path to CharaStats.json in the StreamingAsset Folder
        string savePath = Application.streamingAssetsPath + "/Email.json";

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < concertHalls.Count; i++)
        {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(concertHalls[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void repitoireJSONSave(List<LibraryClass.Repitoire> repitoires)
    {
        // Set the save path to CharaStats.json in the StreamingAsset Folder
        string savePath = Application.streamingAssetsPath + "/Library.json";

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < repitoires.Count; i++)
        {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(repitoires[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void emailJSONSave(List<emailClass.Email> emails)
    {
        // Set the save path to CharaStats.json in the StreamingAsset Folder
        string savePath = Application.streamingAssetsPath + "/Email.json";

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < emails.Count; i++)
        {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(emails[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void npcJSONSave(List<npcClass.NPC> npcs)
    {
        // Set the save path to CharaStats.json in the StreamingAsset Folder
        string savePath = Application.streamingAssetsPath + "/NPC.json";

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < npcs.Count; i++)
        {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(npcs[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void allSave(List<charaClass.Character> chars, List<playerClass.Player> player, List<LibraryClass.Repitoire> repitoires,
        List<emailClass.Email> emails, List<npcClass.NPC> npcs) {
    
        npcJSONSave(chars);
        playerJSONSave(player);
        repitoireJSONSave(repitoires);
        emailJSONSave(emails);
        npcJSONSave(npcs);
    }

}
