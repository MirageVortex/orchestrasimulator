﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveMenu : MonoBehaviour {

    public GameObject menu;

    // Use this for initialization
    void Start () {
        Time.timeScale = 1;
        menu.SetActive(false);
    
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0)){
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                menu.SetActive(true);
                print("game: paused"); // for testing purposes;
                // disable input
            }
            else
            {
                Time.timeScale = 1;
                menu.SetActive(false);
                print("game: resumed"); // for testing purposes;
            }
        }
    }
}
