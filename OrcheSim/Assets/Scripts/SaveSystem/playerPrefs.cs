﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerPrefs : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (!PlayerPrefs.HasKey("volumeMain"))
        {
            PlayerPrefs.SetFloat("volumeMain", 1.0f);
        }
        if (!PlayerPrefs.HasKey("volumeMusic"))
        {
            PlayerPrefs.SetFloat("volumeMusic", 1.0f);
        }
        if (!PlayerPrefs.HasKey("volumeSound"))
        {
            PlayerPrefs.SetFloat("volumeSound", 1.0f);
        }
        if (!PlayerPrefs.HasKey("volumeConcert"))
        {
            PlayerPrefs.SetFloat("volumeConcert", 1.0f);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
