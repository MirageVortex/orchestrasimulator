﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class LibraryTest : MonoBehaviour {

    public Text playable;
    public Text bought;

    public Transform current_orch_list;

    private List<LibraryClass.Repitoire> libraryList;
    private List<charaClass.Character> orchestraList;

    public int[] instrument_count;

    void Start() {
        string libraryPath = Application.streamingAssetsPath + "/Library.json";
        string libraryJSON = File.ReadAllText(libraryPath);

        string orchestraPath = Application.streamingAssetsPath + "/Creature.json";
        string orchestraJSON = File.ReadAllText(orchestraPath);

        libraryList = JsonConvert.DeserializeObject<List<LibraryClass.Repitoire>>(libraryJSON);
        orchestraList = JsonConvert.DeserializeObject<List<charaClass.Character>>(orchestraJSON);

        instrument_count = get_instrument_count(libraryList[0].InstrumentList, orchestraList);

        print(instrument_count[0]);

        for (int i = 0; i < instrument_count.Length; i++) {
            current_orch_list.GetChild(i).GetComponent<Text>().text = instrument_count[i].ToString();
        }

        bool orchestra_check = check_requirements(libraryList[0].InstrumentCount, instrument_count);

        if (orchestra_check) {
            playable.text = "X";
        }

        if (libraryList[0].Purchased) {
            bought.text = "X";
        }
    }

    bool check_requirements(int[] minimum, int[] orchestra) {
        for(int i = 0; i < minimum.Length; i++) {
            if(orchestra[i] < minimum[i]) {
                return false;
            }
        }
        return true;
    }

   // For each repritoire, add required instrument string list and also int list corresponding to the string list
    int[] get_instrument_count(int[] required_instruments, List<charaClass.Character> orchestra) {
        int[] result = new int[required_instruments.Length];
        for (int i = 0; i < required_instruments.Length; i++) {
            for (int x = 0; x < orchestra.Count; x++) {
                if (orchestra[x].Instrument == required_instruments[i]) {
                    result[i]++;
                }
            }
        }
        return result;
    }
}
