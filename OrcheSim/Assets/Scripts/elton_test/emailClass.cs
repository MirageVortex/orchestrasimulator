﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class emailClass : MonoBehaviour {

    [System.Serializable]
    public class Email
    {
        public string SenderName { get; set; }
        public string RecipientName { get; set; }
        public int Type { get; set; } // 0 - Need Reply || 1 - No Reply Needed
        public string Subject { get; set; }
        public string Content { get; set; }
        public Email(string senderName, string recipientName, int type, string subject, string date, string content)
        {
            SenderName = senderName;
            RecipientName = recipientName;
            Type = type;
            Subject = subject;
            Content = content;
        }
    }
}
