﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class emailCreate : MonoBehaviour {

    // Public variables
    public GameObject statsFunct;
    public GameObject ItemList;
    public GameObject npcObj;

    public string firstName;
    public string lastName;

    public Toggle genderBoolMale;
    public Toggle genderBoolFemale;

    private string[] instruments;

    public InputField firstNameInput;
    public InputField lastNameInput;


    // Private variables

    // Email Body
    string header1 = "Mr. ";
    string header2 = "Ms. ";

    string first_email_body = "As a representative of the City Council, I'm" + "\n" +
        "happy to inform you that your orchestra has" + "\n" +
        "been selected as part of the Classical Music" + "\n" +
        "Initiative that the city voted on last year in order" + "\n" +
        "to promote both the awareness and" + "\n" +
        "appreciation of classical music! As such, if you" + "\n" +
        "can prepare a performance by the end of the" + "\n" +
        "month, we would be more than happy to fund" + "\n" +
        "all of its expenditures, including but not limited" + "\n" +
        "to items such as the performer's pay and the" + "\n" +
        "venue. We hope to see a spectacular concert!";

    string closing1 = "Best,";

    // Data List
    private List<emailClass.Email> emailList;
    private List<npcClass.NPC> npcList;

    // Email Data
    string firstEmail;
    string senderName;
    string recipientName;

    string[] titleList;

    // Use this for initialization
    void Start () {
        // initialize the email list
        emailList = new List<emailClass.Email>();

        // get all the titles
        titleList = ItemList.GetComponent<itemList>().Titles;

        // get the npcList
        npcList = npcObj.GetComponent<npcCreate>().npcList;
    }
	
	// Update is called once per frame
	void Update () {
        // change header based on gender
        if (genderBoolMale.isOn) {
            firstEmail = header1;
        } else if (genderBoolFemale.isOn) {
            firstEmail = header2;
        }

        // constantly update the names of sender/recipient
        senderName = npcList[0].FirstName + " " + npcList[0].LastName;

        firstName = firstNameInput.text;
        lastName = lastNameInput.text;

        if (firstName != "" && lastName != "") {
            recipientName = firstName + " " + lastName.Substring(0, 1) + ".";
        }
    }

    public void emailSave () {
        // creates the body body of the email
        firstEmail += recipientName + "\n" + "\n" + first_email_body + "\n" + "\n" + closing1 +
            "\n" + senderName + "\n" + titleList[npcList[0].Title];

        // add a new email to the email list
        emailList.Add(new emailClass.Email(senderName, recipientName, 1, "Sponsorship", "08/01/2017", firstEmail));

        // save
        statsFunct.GetComponent<statsFunctions>().emailJSONSave(emailList);
    }

}
