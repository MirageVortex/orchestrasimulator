﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenAudition : MonoBehaviour {
    public GameObject auditionMenu;

    public void OpenAuditionMenu()
    {
        if (!auditionMenu.activeSelf)
        {
            auditionMenu.SetActive(true);
        }
    }

}
